app.factory('listSeasonsAPI', ['$http', 'BASE_URL', function ($http, BASE_URL) {

    var seasons = [];
    var _getSeasons = function () {
        var req = {
            method: 'GET',
            url: BASE_URL + 'sportspress/v2/seasons?fields=id,name,slug,count,parent,description'
        };
        return $http(req).then(function (response) {
            seasons = response.data;
            return seasons;
        });
    };


    return {
        getSeasons: _getSeasons
    };

}]);
