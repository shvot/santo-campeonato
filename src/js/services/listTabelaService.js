app.factory('listTabelasAPI', ['$http', 'BASE_URL', function ($http, BASE_URL) {
    //Rota para as tabelas com resultados dos jogos
    //http://scapi.cf/index.php/wp-json/sportspress/v2/tabela?fields=id,title,data
    var lista = [];
    var _getTabelas = function () {
        var req3 = {
            method: 'GET',
            url: BASE_URL + 'sportspress/v2/tabela?fields=id,title,data'
        };
        return $http(req3).then(function (response) {
            lista = response.data;
            return lista;
        });
    };


    return {
        getTabelas: _getTabelas
    };

}]);
