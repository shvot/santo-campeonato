app.factory('detailTimeAPI', ['$http', 'BASE_URL',  function ($http, BASE_URL) {
    var data_time = [];
    var _getTime = function (timeid) {
        console.log(timeid);
        var req_time = {
            method: 'GET',
            url: BASE_URL + 'sportspress/v2/times/' + timeid
        };
        return $http(req_time).then(function (response) {
            data_time = response.data;
            return data_time;
        });
    };


    var data_media_time = [];

    var _getMediaTime = function (timeidmedia) {

        console.log(timeidmedia);
        var req2 = {
            method: 'GET',
            url: BASE_URL + 'wp/v2/media/' + timeidmedia
        };
        return $http(req2).then(function (response) {
            data_media_time = response.data;
            return data_media_time;
        });
    };

    return {
        getTime: _getTime,
        getMediaTime: _getMediaTime
    };

}]);
