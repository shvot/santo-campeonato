app.factory('serviceAPI', ['$http', 'BASE_URL', function ($http, BASE_URL) {

    var dados = [];
    var _getDados = function () {
        var req1 = {
            method: 'GET',
            url: BASE_URL + 'acf/v3/config/82'
        };
        return $http(req1).then(function (response) {
            dados = response.data;
            return dados;
        });
    };


    return {
        getDados: _getDados
    };

}]);
