app.factory('listTimesAPI', ['$http', 'BASE_URL', function ($http, BASE_URL) {
    //Rota para as tabelas com resultados dos jogos
    //http://scapi.cf/index.php/wp-json/sportspress/v2/tabela?fields=id,title,data
    var lista = [];
    var _getTimes = function () {
        var req1 = {
            method: 'GET',
            url: BASE_URL + 'sportspress/v2/times'
        };
        return $http(req1).then(function (response) {
            lista = response.data;
            return lista;
        });
    };


    return {
        getTimes: _getTimes
    };

}]);
