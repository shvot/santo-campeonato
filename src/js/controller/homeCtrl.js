(function() {

    var homeCtrl = function homeCtrl(
        $scope,
        $state,
        serviceAPI
    ) {

        var vm = this;
        vm.getInfos = getInfos;

        function getInfos(){
            serviceAPI.getDados()
                .then(function (dados) {
                    $scope.dados = dados.acf;
                    console.log($scope.dados);
                })
                .catch(function (erro) {
                    console.log('error');
                });
        }

        getInfos();


    };

    homeCtrl.$inject = [
        '$scope',
        '$state',
        'serviceAPI'
    ];

    app.controller('homeCtrl', homeCtrl);

})();

