(function() {

var timesCtrl = function timesCtrl(
    $scope,
    $state,
    listTimesAPI,
    detailTimeAPI,
    $stateParams
) {
    var vm = this;
    vm.getListTeams = getListTeams;
    vm.getDetailTeam = getDetailTeam;

    function getListTeams(){
        listTimesAPI.getTimes()
            .then(function (lista) {
                $scope.times = lista;
                console.log(lista);
            })
            .catch(function (erro) {
                console.log('error');
            });
    }

  $scope.goteam = function (id) {
      $state.go('times.timeinfo', {"timeid": id});
      getDetailTeam(id);
  };

    function getDetailTeam(timeid){
        detailTimeAPI.getTime(timeid)
            .then(function (data_time) {
                $scope.time = data_time;
                console.log($scope.time);

                    detailTimeAPI.getMediaTime($scope.time.featured_media)
                        .then(function (data_media_time) {
                            $scope.mediatime = data_media_time;
                            console.log($scope.mediatime);
                        })
                        .catch(function (erro) {
                            $scope.mediatime = {};
                            $scope.mediatime.source_url = '/assets/images/desert.jpg';
                            console.log('error');
                        });
            })
            .catch(function (erro) {
                console.log('error');
            });
    }

};

timesCtrl.$inject = [
    '$scope',
    '$state',
    'listTimesAPI',
    'detailTimeAPI',
    '$stateParams'
];

app.controller('timesCtrl', timesCtrl);

})();

