(function() {

    var tabelasCtrl = function tabelasCtrl(
        $scope,
        $state,
        listTabelasAPI,
        listSeasonsAPI
    ) {

        var vm = this;
        vm.getListTabelas = getListTabelas;
        vm.getIDSeasonbyYear = getIDSeasonbyYear;
        vm.getListSeasons = getListSeasons;

        function getListTabelas(){
            listTabelasAPI.getTabelas()
                .then(function (tabelas) {
                    $scope.tabelas = tabelas;
                    console.log(tabelas);
                })
                .catch(function (erro) {
                    console.log('error');
                });
        }


        function getListSeasons(){
            listSeasonsAPI.getSeasons()
                .then(function (seasons) {
                    $scope.seasons = seasons;
                })
                .catch(function (erro) {
                    console.log('error');
                });
        }        



        function getIDSeasonbyYear(year){
            $scope.ano = year;
            listSeasonsAPI.getSeasons()
                .then(function (seasonByYear) {
                    $scope.seasons = seasonByYear;

                    var slug_ano = 's' + year;

                    var resultadomap = seasonByYear.filter(function(elem){
                            return elem.slug === slug_ano;
                        });

                   // console.log('oi',resultadomap);
                })
                .catch(function (erro) {
                    console.log('error');
                });
        }

        getIDSeasonbyYear(2018);



    };

    tabelasCtrl.$inject = [
        '$scope',
        '$state',
        'listTabelasAPI',
        'listSeasonsAPI'
    ];

    app.controller('tabelasCtrl', tabelasCtrl);

})();

